package EDU.BR;

public class Main {

    public static void main(String[] args) {
        FilaBanco fila = new FilaBanco();
        fila.insert("Fulano", 20);
        fila.insert("Sicrano", 23);
        fila.insert("Beltrano", 45);
        fila.insert("Vovó", 65);
        fila.insert("Tio", 48);
        fila.insert(new Pessoa("Primo", 18));



        Pessoa p ;
       // fila.mudarIdade("Tio", 90);

        fila.mudarIdade("Vovó", 3);


        while (fila.getSize() > 0) {
            p = fila.peek();
            System.out.println("Atendendo " + p.getNome() + ", idade: " + p.getIdade());
            fila.remove();
        }

    }
}